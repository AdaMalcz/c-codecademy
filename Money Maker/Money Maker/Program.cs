﻿using System;
/* 
MoneyMaker converts given value in cents to the smallest possible amount of coins.
  - bronze coin = 1 cent
  - silver coin = 5 cens
  - gold coin = 10 cents 
*/
namespace MoneyMaker
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            // Coins values:
            int bronzeValue = 1;
            int silverValue = 5;
            int goldValue = 10;

            // User amount value
            Console.WriteLine("Welcome to Money Maker!\n");
            Console.Write("Enter the value (in cents) to convert: ");
            double amount = Convert.ToDouble(Console.ReadLine());

            // Convertion:
            double goldCoins = Math.Floor(amount / goldValue);
            double centsLeft = amount % goldValue;
            double silverCoins = Math.Floor(centsLeft / silverValue);
            double centsRest = centsLeft % silverValue;
            double bronzeCoins = Math.Floor(centsRest / bronzeValue);

            // Printing results:
            Console.WriteLine($"{amount} cents is equal to {goldCoins} gold coins, {silverCoins} silver coins and {bronzeCoins} bronze coins.");

            // Print a goodbye message:
            Console.WriteLine("The content of your pouch is impressive! Press enter to leave.");
            Console.ReadLine();
        }
    }
}