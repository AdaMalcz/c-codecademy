﻿using System;

namespace ChooseYourOwnAdventure
{
    class Program
    {
        static void Main(string[] args)
        {
            /* THE MYSTERIOUS NOISE */

            // Enter user's name:
            Console.Write("What is your name?: ");
            string name = Console.ReadLine();
            Console.WriteLine($"Hello, {name}! Welcome to our story.");

            // Start the story:
            Console.WriteLine();
            Console.WriteLine("It begins on a cold rainy night. You're sitting in your room and hear a noise coming from down the hall. Do you go investigate?");
            Console.Write("Type YES or NO: ");

            // First choice:
            string noiseChoice = Console.ReadLine().ToUpper();
            if (noiseChoice == "NO")
            {
                Console.WriteLine();
                Console.WriteLine("Not much of an adventure if we don't leave our room! THE END.");
            }
            else if (noiseChoice == "YES")
            {
                Console.WriteLine();
                Console.WriteLine("You walk into the hallway and see a light coming from under a door down the hall. You walk towards it. Do you open it or knock?");

                // Second choice:
                Console.Write("Type OPEN or KNOCK: ");
                string doorChoice = Console.ReadLine().ToUpper();
                if (doorChoice == "KNOCK")
                {
                    // The riddle:
                    Console.WriteLine();
                    Console.WriteLine("A voice behind the door speaks. It says, \"Answer this riddle: \" \"Poor people have it. Rich people need it. If you eat it you die. What is it?\"");
                    Console.Write("Type your answer: ");
                    string riddleAnswer = Console.ReadLine().ToUpper();
                    if (riddleAnswer == "NOTHING")
                    {
                        Console.WriteLine();
                        Console.WriteLine("The door opens and NOTHING is there. You turn off the light and run back to your room and lock the door. THE END.");
                    }
                    else
                    {
                        Console.WriteLine();
                        Console.WriteLine("You answered incorrectly. The door doesn't open. THE END.");
                    }
                }
                else if (doorChoice == "OPEN")
                {
                    // Try to open the door:
                    Console.WriteLine();
                    Console.WriteLine("The door is locked! See if one of your three keys will open it.");
                    Console.Write("Enter a number (1-3): ");
                    string keyChoice = Console.ReadLine().ToUpper();
                    switch (keyChoice)
                    {
                        // Choice of the key:
                        case "1":
                            Console.WriteLine();
                            Console.WriteLine("You choose the first key. Lucky choice! The door opens and NOTHING is there. Strange... THE END.");
                            break;
                        case "2":
                            Console.WriteLine();
                            Console.WriteLine("You choose the second key. The door doesn't open. THE END.");
                            break;
                        case "3":
                            Console.WriteLine();
                            Console.WriteLine("You choose the third key. The door doesn't open. THE END.");
                            break;
                        default:
                            Console.WriteLine();
                            Console.WriteLine($"You don't have a key {keyChoice}. THE END.");
                            break;
                    }
                }
                else
                {
                    Console.WriteLine();
                    Console.WriteLine("You should pass KNOCK or OPEN! THE END");
                }
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("You should pass YES or NO! THE END");
            }

            // Exit the program:
            Console.WriteLine("Press enter to exit");
            Console.ReadLine();
        }
    }
}