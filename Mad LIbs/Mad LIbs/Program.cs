﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mad_Libs
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
      This program is C# implementation of Mad Libs word game.
      Author: Adam
      */


            // Let the user know that the program is starting:
            Console.WriteLine("Let the Mad Libs begin!");

            // Give the Mad Lib a title:
            string title = "The Mad Mad Libs Lib";
            Console.WriteLine(title);

            // Define user input and variables:
            Console.Write("Enter a main character name: ");
            string characterName = Console.ReadLine();
            Console.Write("Enter an adjective: ");
            string adjectiveOne = Console.ReadLine();
            Console.Write("Enter an adjective: ");
            string adjectiveTwo = Console.ReadLine();
            Console.Write("Enter an adjective: ");
            string adjectiveThree = Console.ReadLine();
            Console.Write("Enter a verb: ");
            string verb = Console.ReadLine();
            Console.Write("Enter a noun: ");
            string nounOne = Console.ReadLine();
            Console.Write("Enter a noun: ");
            string nounTwo = Console.ReadLine();
            Console.Write("Enter an animal: ");
            string animal = Console.ReadLine();
            Console.Write("Enter a food: ");
            string food = Console.ReadLine();
            Console.Write("Enter a fruit: ");
            string fruit = Console.ReadLine();
            Console.Write("Enter a superhero: ");
            string superhero = Console.ReadLine();
            Console.Write("Enter a country: ");
            string country = Console.ReadLine();
            Console.Write("Enter a dessert: ");
            string dessert = Console.ReadLine();
            Console.Write("Enter a year: ");
            string year = Console.ReadLine();


            // The template for the story:
            string story = $"This morning {characterName} woke up feeling {adjectiveOne}. 'It is going to be a {adjectiveTwo} day!' Outside, a bunch of {animal}s were protesting to keep {food} in stores. They began to {verb} to the rhythm of the {nounOne}, which made all the {fruit}s very {adjectiveThree}. Concerned, {characterName} texted {superhero}, who flew {characterName} to {country} and dropped {characterName} in a puddle of frozen {dessert}. {characterName} woke up in the year {year}, in a world where {nounTwo}s ruled the world.";


            // Print the story:
            Console.WriteLine(story);

            // Print a goodbye message:
            Console.WriteLine("What a hilarious story! Press enter to leave and... maybe another try?");
            Console.ReadLine();
        }
    }
}
