﻿using System;
/*
Password Checker project uses tool Tools.Contains(targer, list) which was provided by Code Cademy online platform, so the code below doesn't work for now.
I plan to make it work soon by:
    1. Write my own tool (need to learn more of C#)
        or
    2. Use regular expressions [https://docs.microsoft.com/en-us/dotnet/api/system.text.regularexpressions.regex?view=netframework-4.8#examples] (need to familiarize with the documentation)
 */
namespace PasswordChecker
{
    class Program
    {
        public static void Main(string[] args)
        {
            // Password standards:
            int minLength = 8;
            string upperCase = "QWERTYUIOPASDFGHJKLZXCVBNM";
            string lowerCase = "qwertyuiopasdfghjklzxcvbnm";
            string digits = "1234567890";
            string specialChars = "!@#$%^&*?-+=/\\|{}[]()_,.<>";

            // Capturing user password:
            Console.Write("Enter your password: ");
            string userPassword = Console.ReadLine();

            // Measuring password strength
            int score = 0;
            if (userPassword.Length >= minLength)
            {
                score++;
            }
            if (Tools.Contains(userPassword, upperCase))
            {
                score++;
            }
            if (Tools.Contains(userPassword, lowerCase))
            {
                score++;
            }
            if (Tools.Contains(userPassword, digits))
            {
                score++;
            }
            if (Tools.Contains(userPassword, specialChars))
            {
                score++;
            }
            if (userPassword == "password" || userPassword == "1234")
            {
                score = 0;
            }
            Console.WriteLine(score);

            // Print feedback message:
            switch (score)
            {
                case 5:
                case 4:
                    Console.WriteLine("Your password is extremely strong!");
                    break;
                case 3:
                    Console.WriteLine("Your password is strong!");
                    break;
                case 2:
                    Console.WriteLine("Your password is medium strong!");
                    break;
                case 1:
                    Console.WriteLine("Your password is weak!");
                    break;
                default:
                    Console.WriteLine("Your password doesn't meet any of the standards!");
                    break;
            }
            // Exit program
            Console.ReadLine();
        }
    }
}