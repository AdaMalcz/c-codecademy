﻿using System;

namespace ExquisiteCorpse
{
    class Program
    {
        static void Main(string[] args)
        {
            // Creates ASCII style creatures from given (or random) creature body part. Future extension planned.
            Console.WriteLine("Give me the monster with bug head:");
            BuildACreature("bug", "monster", "monster");
            Console.WriteLine();
            Console.WriteLine("Give me some random creature:");
            RandomMode();

            // Exit by pressing enter
            Console.ReadLine();
        }

        static void BuildACreature(string head, string body, string feet)
        {
            int numHead = TranslateToNumber(head);
            int numBody = TranslateToNumber(body);
            int numFeet = TranslateToNumber(feet);
            SwitchCase(numHead, numBody, numFeet);
        }

        static void RandomMode()
        {
            Random randomNumber = new Random();
            int randomHead = randomNumber.Next(1, 4);
            int randomBody = randomNumber.Next(1, 4);
            int randomFeet = randomNumber.Next(1, 4);
            SwitchCase(randomHead, randomBody, randomFeet);
        }

        static void SwitchCase(int head, int body, int feet)
        {
            switch (head)
            {
                case 1:
                    BugHead();
                    break;
                case 2:
                    GhostHead();
                    break;
                case 3:
                    MonsterHead();
                    break;
            }
            switch (body)
            {
                case 1:
                    BugBody();
                    break;
                case 2:
                    GhostBody();
                    break;
                case 3:
                    MonsterBody();
                    break;
            }
            switch (feet)
            {
                case 1:
                    BugFeet();
                    break;
                case 2:
                    GhostFeet();
                    break;
                case 3:
                    MonsterFeet();
                    break;
            }
        }

        static int TranslateToNumber(string creature)
        {
            switch (creature)
            {
                case "bug":
                    return 1;
                case "ghost":
                    return 2;
                case "monster":
                    return 3;
                default:
                    return 2;
            }
        }

        static void GhostHead()
        {
            Console.WriteLine("     ..-..");
            Console.WriteLine("    ( o o )");
            Console.WriteLine("    |  O  |");
        }

        static void GhostBody()
        {
            Console.WriteLine("    |     |");
            Console.WriteLine("    |     |");
            Console.WriteLine("    |     |");
        }

        static void GhostFeet()
        {
            Console.WriteLine("    |     |");
            Console.WriteLine("    |     |");
            Console.WriteLine("    '~~~~~'");
        }

        static void BugHead()
        {
            Console.WriteLine("     /   \\");
            Console.WriteLine("     \\. ./");
            Console.WriteLine("    (o + o)");
        }

        static void BugBody()
        {
            Console.WriteLine("  --|  |  |--");
            Console.WriteLine("  --|  |  |--");
            Console.WriteLine("  --|  |  |--");
        }

        static void BugFeet()
        {
            Console.WriteLine("     v   v");
            Console.WriteLine("     *****");
        }

        static void MonsterHead()
        {
            Console.WriteLine("     _____");
            Console.WriteLine(" .-,;='';_),-.");
            Console.WriteLine("  \\_\\(),()/_/");
            Console.WriteLine("　  (,___,)");
        }

        static void MonsterBody()
        {
            Console.WriteLine("   ,-/`~`\\-,___");
            Console.WriteLine("  / /).:.('--._)");
            Console.WriteLine(" {_[ (_,_)");
        }

        static void MonsterFeet()
        {
            Console.WriteLine("    |  Y  |");
            Console.WriteLine("   /   |   \\");
            Console.WriteLine("   \"\"\"\" \"\"\"\"");
        }
    }
}

/* 
Code Cademy extension ideas:

1. Extend the BuildACreature() method so that all of its parameters are optional. It should assign a random body part if a parameter is not specified.

2. Recreate this program so that rather than outputting a creature immediately, it prompts a user to select which parts of each creature to use to build a new creature.

3. Add a starting mode, so a user can select whether to randomly generate a creature or create one manually.

4. Use ASCII art archive to add other body parts to the program.
    https://www.asciiart.eu/animals
*/
