﻿using System;

namespace CaesarCipher
{
    class Program
    {
        static void Main(string[] args)
        {
            char[] alphabet = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };

            // Passing a message to encrypt:
            Console.Write("Write a message you want to encode: ");
            string userMessage = Console.ReadLine();

            // Encryption
            char[] secretMessage = userMessage.ToCharArray();
            char[] encryptedMessage = new char[secretMessage.Length];

            for (int i = 0; i < secretMessage.Length; i++)
            {
                char character = secretMessage[i];
                int characterIndex = Array.IndexOf(alphabet, character);
                int newCharacterIndex = (characterIndex + 3) % 26;
                char newCharacter = alphabet[newCharacterIndex];
                encryptedMessage[i] = newCharacter;
            }

            string userEncryptedMessage = String.Join("", encryptedMessage);
            Console.WriteLine(userEncryptedMessage);

            // Exit program
            Console.ReadLine();

        }
    }
}

/* 
Code Cademy extension ideas:
1. The app doesn’t work with uppercase letters. Fix that by converting any message to lowercase.

2. The app doesn’t work with symbols, like ! or ?. Skip any symbols in your loop so that they are not encrypted.

3. Rewrite the loop as a method Encrypt() which takes a character array and key and returns an encrypted character array.

4. Write a Decrypt() method which takes a character array and key and returns a decrypted character array.

    CA Hints:   Convert strings to lowercase with ToLower():
                // quiet will be "hey"
                string loud = "HEY";
                string quiet = loud.ToLower();
            
                Use if and else statements to perform different actions based on different character types.

                Check if a character is a letter using Char.IsLetter():
                // b will be false
                char ch = "?";
                bool b = Char.IsLetter(ch);

My own extension ideas:
6. Handle spaces.

7. Rewrite code as method ==>> console app.
*/
