﻿using System;

namespace ArchitectArithmetic
{
    class Program
    {
        static double RectangleArea(double length, double width)
        {
            return length * width;
        }

        static double CircleArea(double radius)
        {
            return Math.PI * Math.Pow(radius, 2);
        }

        static double TriangleArea(double bottom, double height)
        {
            return 0.5 * bottom * height;
        }

        public static void Main(string[] args)
        {
            // Testing section:
            //Console.WriteLine(RectangleArea(4, 5)); //should print 20 - OK!
            //Console.WriteLine(CircleArea(4)); //should print 50 - OK!
            //Console.WriteLine(TriangleArea(10, 9)); //should print 45 - OK!

            // Approximation of floor area (Teotihuacan, Mexico)
            // floor plan with dimensions provided by Code Cademy on the online platform
            double teotihuacanArea = RectangleArea(1500, 2500) + CircleArea(375) / 2 + TriangleArea(750, 500);
            Console.WriteLine($"The floor area of Teotihuacan (Mexico) is approximately {teotihuacanArea}m^2.\n");

            // Materials cost estimation
            double materialCost = 180; //mexican pesos per m^2
            double floorCost = Math.Round(teotihuacanArea * materialCost, 2);
            Console.WriteLine($"Total cost of the materials is {floorCost} mexican pesos.");

            // Press enter to exit
            Console.ReadLine();

/* 
CodeCademy extension ideas:

1. Make the entire program a method so that you can execute it in your Main() method with one line:
    CalculateTotalCost();
    tip: For the first challenge, you’ll need to define a method named CalculateTotalCost() that returns a string like "The plan for that monument costs 748510782.02 pesos!".

2. Determine the total cost for the Taj Mahal in Agra, India and the Al-Masjid al-haram (Great Mosque) in Mecca, Saudi Arabia.
    tip: Taj Mahal: calculate the area of the complete rectangle, then subtract the area of the four triangles.
Great Mosque of Mecca: calculate the area of the two complete rectangles, then subtract the area of the triangle.

3. Use conditional statements and Console.ReadLine() to allow users to pick which monument for which they’d like to calculate a cost.
    "What monument would you like to work with? Teotihuacan The plan for that monument costs 748510782.02 pesos!"
    tip: use switch or if...else if...else statements.


My own extension ideas:

4. Unit tests for methods
5. Different materials with different costs
6. Display floor plan as image after calculation
7. Values display format (748510782.02 => 74 8510782,02)
*/
        }
    }
}
