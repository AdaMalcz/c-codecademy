﻿using System;

namespace TrueOrFalse
{
    class Program
    {
        static void Main(string[] args)
        {
            // Quiz app (true or false) 
            Console.WriteLine("Welcome to 'True or False?'\nPress enter to begin:");
            string entry = Console.ReadLine();

            string[] questions = new string[] { "Roses are red", "Violets are green", "Elephant is an insect" };
            bool[] answers = new bool[] { true, false, false };
            bool[] responses = new bool[questions.Length];

            // Check arrays lengths:
            if (questions.Length != answers.Length)
            {
                Console.WriteLine("Questions and Answers arrays are not equal in lenght!");
            }

            // Asking questions:
            int askingIndex = 0;

            foreach (string question in questions)
            {
                string input;
                bool isBool;
                bool inputBool;

                Console.WriteLine();
                Console.WriteLine(question);
                Console.Write("True or false? ");
                input = Console.ReadLine();

                isBool = Boolean.TryParse(input, out inputBool);

                while (!isBool)
                {
                    Console.WriteLine("Please respond with 'true' or 'false'.");
                    Console.Write("True or false? ");
                    input = Console.ReadLine();
                    isBool = Boolean.TryParse(input, out inputBool);
                }

                responses[askingIndex] = Convert.ToBoolean(input);
                askingIndex++;
            }

            // Checking answers:
            int scoringIndex = 0;
            int score = 0;

            Console.WriteLine();
            foreach (bool answer in answers)
            {
                bool userAnswer = responses[scoringIndex];
                Console.WriteLine($"[Q{scoringIndex}]: {questions[scoringIndex]}\n\t Your answer: {userAnswer} \t| Correct answer: {answer}");
                if (userAnswer == answer)
                {
                    score++;
                }
                scoringIndex++;
            }
            Console.WriteLine();
            Console.WriteLine($"You got {score} out of {questions.Length} correct!");

            // Exit program
            Console.ReadLine();

        }
    }
}

/* 
Code Cademy extension ideas:

1. If you wanted to make multiple quizzes, you would have to type out the same code multiple times. Fix this issue by refactoring your code into a method RunQuiz(). It should take two arguments: a string[] array of questions and boolean[] array of answers. It should have the same behavior as your current app.

    string[] questions = new string[]
    {
      ...
    };
    bool[] answers = new bool[]
    {
      ...
    };
    RunQuiz(questions, answers);

*/