﻿using System;

namespace DatingProfile
{
    class Profile
    {
        // FIELDS
        private string name;
        private int age;
        private string city;
        private string country;
        private string pronouns;
        private string[] hobbies;

        // CONSTRUCTORS
        public Profile(string name, int age, string city, string country, string pronouns)
        {
            this.name = name;
            this.age = age;
            this.city = city;
            this.country = country;
            this.pronouns = pronouns;
        }

        // METHODS
        public string ViewProfile()
        {
            string profileInfo = $"Name: {name}\nAge: {age}\nCity: {city}\nCountry: {country}\nPronouns: {pronouns}\nHobbies: ";
            foreach (string hobbie in hobbies)
            {
                profileInfo += $"\n  * {hobbie}";
            }
            return profileInfo;
        }

        public void SetHobbies(string[] hobbies)
        {
            this.hobbies = hobbies;
        }

    }
}
