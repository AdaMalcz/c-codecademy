﻿using System;

namespace DatingProfile
{
    class Program
    {
        static void Main(string[] args)
        {
            Profile sam = new Profile("Sam Drakkila", 30, "New York", "USA", "he/him");
            sam.SetHobbies(new string[] { "listening to audiobooks and podcasts", "playing rec sports like bowling and kickball", "writing a speculative fiction novel", "reading advice columns" });
            Console.WriteLine(sam.ViewProfile());

            // Exit program
            Console.ReadLine();
        }
    }
}

/*
Code Cademy extension ideas:
1. If you call ViewProfile() before calling SetHobbies(), you’ll get an error because the hobbies field isn’t set to any value. Fix the class so that you can call ViewProfile() without adding hobbies.

2. Convert the fields into private properties and add validation. For example, users must be at least 18 years of age.

3. Some users may create a profile with just a name and age. Use optional parameters or create a constructor overload to handle those issues.

Hints:
    *Avoiding errors — To avoid the errors in ViewProfile():

    In the constructor, give hobbies a default value of an array of length 0.
    In ViewProfile() only append hobbies in if hobbies.Length is greater than 0.
    
    *Adding properties — A private property is defined like:

    private string Name
    { 
      get { omitted }
      set { omitted  }
    }
    
    *Partial profiles I — Optional parameters must have a default value like:

    public Recipe(string name = "n/a")
    
    *Partials profiles II — When overloading constructors, avoid code duplication by using : this(), like:

    public Recipe() : this("n/a")

My own extension ideas:
4. Console profile creator
 */
